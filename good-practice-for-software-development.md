# Good Practice for Software Development

## 1. Which point(s) were new to you in the section of good practice for software development?

- Some points that might be new to me based on the good practices for software development are as follows:

    1. **Writing down what you need to do:** You learned that talking about what the software should do is important, but it's also a good idea to write it down right away. This way, everyone on your team can read it, make sure it's clear, and give feedback. Even if your company doesn't have a fancy tool, just writing it in a doc and sharing it works.

    2. **Asking questions the right way:** Getting help is important, but you learned a neat trick! When you ask a question, try to explain what's wrong clearly. Show any solutions you already tried, and use pictures or short videos if it helps. This way, whoever is helping you can understand the problem much faster.

    3. **Staying focused while working from home:** Working from home is cool, but there are lots of distractions! You learned some ways to stay on track. Take breaks, but turn off your phone and block distracting websites. Maybe even try a timer to work in focused bursts! These will help you get more done in less time.

## 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

- Based on the good practices for software development, here are some areas we might want to improve on:

    **1. Communication:**

    - **Asking clear questions:** The guide mentioned a good tip for getting help. When we have a question, explain the problem simply and mention what we've already tried to fix it. If pictures or a short video can help explain, that's even better. This will make it much easier for someone to assist us.

    **2. Focus while working:**

    - **Avoiding distractions:** The guide talks about staying on track while working from home, which can be tricky. One tip is to turn off our phone and block distracting websites. We can also try setting a timer to work in focused bursts with short breaks in between.