# SOLID: The First Five Principles of Object-Oriented Design

SOLID is an acronym representing the first five object-oriented design (OOD) principles proposed by Robert C. Martin. It serves as a set of guidelines for creating software that is flexible, maintainable, and robust. By adhering to these principles, developers can organize their code in a way that makes it easier to understand, modify, and extend over time. Let's break down each of these principles:

1. **Single Responsibility Principle (SRP)**:
   - Each part of your code should have a single responsibility, meaning it should only do one thing. This principle helps maintain clean, focused code by ensuring that each component is responsible for a specific task. It promotes modularity and reduces the risk of unintended side effects.

2. **Open/Closed Principle (OCP)**:
   - Software entities should be open for extension but closed for modification. In other words, you should be able to add new functionality to your code without altering existing code. This promotes code reuse and scalability while minimizing the risk of introducing bugs in previously working code.

3. **Liskov Substitution Principle (LSP)**:
   - Objects of a superclass should be replaceable with objects of a subclass without affecting the correctness of the program. This principle ensures that derived classes can be used interchangeably with their base classes, maintaining the behavior expected by clients of the code.

4. **Interface Segregation Principle (ISP)**:
   - Clients should not be forced to depend on interfaces they do not use. Instead, interfaces should be tailored to the specific needs of clients, avoiding unnecessary dependencies. This promotes cohesion and flexibility in code design.

5. **Dependency Inversion Principle (DIP)**:
   - High-level modules should not depend on low-level modules. Both should depend on abstractions. This principle encourages decoupling between modules by promoting the use of abstractions and interfaces, rather than concrete implementations. It facilitates easier maintenance, testing, and modification of code.

Following these principles results in code that is easier to understand, maintain, and extend, leading to improved software quality and developer productivity.

## References
* [SOLID Principles - By kudvenkat](https://www.youtube.com/playlist?list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme)

* [DigitalOcean - S.O.L.I.D: The First Five Principles of Object Oriented Design](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)