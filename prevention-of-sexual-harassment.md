# Prevention of Sexual Harassment

## 1. What kinds of behaviour cause sexual harassment?

- Sexual harassment happens when someone does or says things that make another person feel uncomfortable, embarrassed, or scared.

- This can include unwanted touching, comments about someone's body or looks, making sexual jokes, or sending inappropriate messages or pictures.

- It's important to remember that what might seem like harmless flirting to one person can be harassment to another.

- Respect for boundaries and consent is key in all interactions.

- If someone tells you they're uncomfortable with your behavior, it's crucial to listen and stop.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- If I encounter or witness any inappropriate behavior, I would take action immediately.

- I'd calmly address the situation, emphasizing respect and boundaries.

- If the behavior continues, I'd report it to the appropriate authority, ensuring proper documentation and support for those affected.

- It's crucial to create a safe environment for everyone, where harassment or misconduct is not tolerated.

- Open communication and swift action are key to addressing such issues effectively and promoting a culture of respect and inclusion.