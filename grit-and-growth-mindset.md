# Grit and Growth Mindset

## 1. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=H14bBuluwB8) in a few (1 or 2) lines. Use your own words.

- The speaker shares how leaving a demanding job to teach revealed that success isn't just about IQ but also about grit, defined as passion and perseverance for long-term goals.

- They emphasize the need to cultivate grit in students through strategies like growth mindset, highlighting the importance of ongoing research and testing in education.

## 2. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=75GFzikmRY0) in a few (1 or 2) lines. Use your own words.

- The video discusses the concept of growth mindset, popularized by Carol Dweck, contrasting it with fixed mindset.

- It emphasizes how beliefs and focus influence learning behaviors, with growth mindset fostering effort, resilience, and learning from mistakes, while fixed mindset leads to avoidance of challenges and defensiveness towards feedback.

## 3. What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?

- The Internal Locus of Control is the belief that one has control over their own life and outcomes.

- It's the conviction that personal actions and efforts directly influence results, rather than attributing success or failure to external factors beyond one's control.

- The key point in the video is that individuals with an internal locus of control tend to be more motivated and resilient.

- They take responsibility for their actions and believe that their efforts can lead to positive outcomes.

- In contrast, those with an external locus of control tend to feel less motivated and may give up more easily, as they attribute success or failure to external forces rather than their own efforts.

- The video emphasizes the importance of developing an internal locus of control to maintain motivation and achieve success in various aspects of life.

## 4. What are the key points mentioned by [speaker](https://www.youtube.com/watch?v=9DVdclX6NzY) to build growth mindset (explanation not needed).

- The key points mentioned by the speaker to build a growth mindset:

    1. Believe in your ability to figure things out.

    2. Question your assumptions.

    3. Develop your own life curriculum.

    4. Honor the struggle.

- These points emphasize the importance of believing in oneself, challenging limiting beliefs, taking ownership of one's learning journey and embracing challenges as opportunities for growth.

## 5. What are your ideas to take action and build Growth Mindset?

- Building a growth mindset is essential for continuous improvement and success in any field, especially in something as dynamic as coding. Here are some actionable ideas to cultivate a growth mindset:

    1. **Embrace Challenges:** Instead of avoiding difficult tasks, see them as opportunities for growth. Challenge yourself with projects or problems slightly beyond your current skill level.

    2. **Learn from Failure:** View mistakes and failures as valuable learning experiences. Analyze what went wrong, how you can improve, and apply those lessons in future endeavors.

    3. **Seek Feedback:** Actively seek feedback from peers, mentors, or online communities. Constructive criticism helps you identify areas for improvement and accelerates your learning process.

    4. **Celebrate Progress:** Acknowledge and celebrate your progress, no matter how small. Recognize the effort you've put in and the milestones you've achieved along the way.

    5. **Foster Curiosity:** Stay curious and hungry for knowledge. Explore new technologies, tools, and methodologies to broaden your skill set and deepen your understanding.

    6. **Practice Mindfulness:** Stay present and focused on the task at hand. Practice mindfulness techniques to manage stress, enhance concentration, and promote clarity of thought.

    7. **Encourage Learning Communities:** Surround yourself with like-minded individuals who share your passion for learning and growth. Engage in discussions, collaborate on projects, and support each other's development.

- By adopting these strategies and integrating them into your daily routine, you can cultivate a growth mindset that empowers you to thrive in your coding journey and beyond.

- Growth is a continuous process, so keep challenging yourself and never stop learning.