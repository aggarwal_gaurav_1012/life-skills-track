# Energy Management

## 1. What are the activities you do that make you relax - Calm quadrant?

- When I want to feel relaxed, I like to do simple things that make me calm.

- I enjoy reading books, or I'll have a cup of tea and listen to gentle music.

- Going for a walk outside in nature helps me feel peaceful and happy.

- When I really need to unwind, taking a warm bath with nice-smelling candles is so soothing.

- These little activities help me feel calm and happy inside.

## 2. When do you find getting into the Stress quadrant?

- When life gets super busy and everything feels overwhelming, that's when I find myself slipping into the Stress quadrant.

- It's like when you have a lot of things to do and not enough time to do them.

- Maybe it's because of work piling up, or maybe there are too many responsibilities to handle all at once.

- Sometimes, unexpected problems pop up, and it feels like there's no way to solve them.

- That's when stress creeps in, making it hard to think clearly or relax.

- It's like being stuck in a traffic jam of worries and not knowing how to find the way out.

## 3. How do you understand if you are in the Excitement quadrant?

- I can tell I'm in the excitement quadrant when you feel super energetic and pumped up!

- It's like my heart is racing with joy and everything seems so thrilling.

- I might notice myself smiling a lot, feeling eager to try new things, or finding it hard to sit still.

- My body might feel a bit woried or nervous, but in a good way, like I'm ready to tackle anything that comes my way.

- It's a fun and exciting feeling that makes me want to jump up and down with excitement.

## 4. Paraphrase the Sleep is your Superpower [video](https://www.youtube.com/watch?v=5MuIMqhT8DM) in your own words in brief. Only the points, no explanation.

- Sleep is important for our health and well-being.

- Sleep deprivation has negative consequences for both the brain and the body.

- Sleep is necessary for consolidating memories and learning.

- People who are sleep deprived are not able to absorb new information as well as those who are well-rested.

- Deep sleep is especially important for memory consolidation.

- Ageing is associated with a decline in sleep quality, and this can contribute to cognitive decline and memory loss.

- Sleep deprivation is also a risk factor for Alzheimer's disease.

- There are ways to improve sleep quality, such as avoiding alcohol and caffeine before bed, maintaining a regular sleep schedule, and keeping the bedroom cool.

## 5. What are some ideas that you can implement to sleep better?

- Improving sleep can be done in many simple ways.

- First, make sure your bedroom is comfortable and dark, as light can disturb sleep.

- Try to stick to a consistent sleep schedule, going to bed and waking up at the same time every day.

- Avoiding caffeine and heavy meals close to bedtime can also help.

- Creating a relaxing bedtime routine, like reading or taking a warm bath, signals to your body that it's time to wind down.

- Additionally, limit screen time before bed, as the blue light from electronics can disrupt sleep patterns.

- By making these small changes, you can set yourself up for a better night's rest.

## 6. Paraphrase the [video](https://www.youtube.com/watch?v=BHY0FxzoKZE) - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- According to the video, here are 5 benefits of exercise for the brain:

    1. Exercise increases chemicals in the brain that make you feel happy and focused.

    2. Exercise makes the part of the brain that helps you remember things bigger.

    3. Exercise helps you think better and pay attention more.

    4. Exercise helps protect your brain from diseases like Alzheimer's and dementia.

    5. You only need 30 minutes of exercise 3-4 times a week to get these benefits.

## 7. What are some steps you can take to exercise more?

- Exercising more can be as simple as taking small steps each day.
    
- Start by incorporating short walks into your routine, maybe during lunch breaks or after dinner.
    
- Find activities you enjoy, like dancing, swimming, or playing a sport with friends.
    
- Setting achievable goals, like aiming for 30 minutes of activity most days of the week, can help keep you on track.
    
- Try to make it fun! Maybe join a group fitness class or explore outdoor trails. 
    
- Even small changes, like taking the stairs instead of the elevator or parking farther away, can add up to big benefits for your health.