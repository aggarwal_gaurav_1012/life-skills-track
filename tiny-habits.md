# Tiny Habits

## 1. In this [video](https://www.youtube.com/watch?v=AdKUJxjn-R8), what was the most interesting story or idea for you?

- According to the video, the most interesting idea is the concept of introducing tiny habits and how they can lead to lasting behavioral change.

- The speaker, BJ Fogg, argues that relying on motivation and willpower to change behavior is not an effective strategy in the long term.

- Instead, he proposes focusing on creating tiny habits that are easy to perform and integrate into existing routines.

## 2. How can you use B = MAP to make making new habits easier? What are M, A and P.

- B = MAP is a handy trick to remember the three ingredients for sticking to new habits. The breakdown is as follows:

- **M = Motivation:** This is your "why." Why do you want to floss daily or finally learn that new language? The stronger your desire, the easier it is to stick with it.

- **A = Ability:**  Think of this as making the habit ridiculously easy. Start with tiny, achievable steps – two minutes of flossing instead of ten, or just five new vocabulary words a day. Tiny wins keep you going!

- **P = Prompt:** This is your reminder. Maybe you floss right after brushing your teeth, or set a phone alarm for your language practice. Prompts nudge you in the right direction, especially when motivation dips.

## 3. Why it is important to "Shine" or Celebrate after each successful completion of habit?

- Celebrating those little wins – "shining" as you put it.

- It releases a feel-good chemical called dopamine, which makes you feel happy and motivated.

- That positive feeling reinforces the habit you just completed, making it more likely you'll stick with it in the future.

- In short, celebrating success makes success more likely to become a habit.

## 4. In this [video](https://www.youtube.com/watch?v=mNeXuCYiE0U), what was the most interesting story or idea for you?

- According to the video, the most interesting idea is the concept of becoming someone new through habit formation.

- The speaker, James Clear, argues that the small changes we make in our daily routines can accumulate over time and shape our identity.

- The key takeaway is that our habits are a reflection of who we are.

- Every time we perform a habit, we are reinforcing our self-image.

- By changing our habits, we can gradually change our identity.

- If you want to become a writer, you can start by writing every day, even if it's just for a short period.

- The speaker also provides a four-stage framework for building better habits: Noticing, Wanting, Doing and Liking.

- By following these steps, you can increase your chances of successfully forming a new habit and ultimately transforming yourself into the person you want to be.

## 5. What is the book's perspective about Identity?

- According to the video, the book Atomic Habits by James Clear argues that identity is the North Star of habit change.

- This means that long-term changes are best achieved by focusing on who you want to be rather than just what you want to do.

## 6. Write about the book's perspective on how to make a habit easier to do?

- The book outlines four laws of behavior change:

    1. **Make it obvious:** Design your environment around your cues.

    2. **Make it attractive:** Make habits attractive by associating them with a reward.

    3. **Make it easy:** Reduce friction by priming your environment for the habits you want to develop.

    4. **Make it satisfying:** Tie your habits to immediate rewards to make them more likely to stick.

- The key takeaway from the book is that small habits compound over time and can lead to significant change.

- It is more important to focus on building systems and developing an identity as someone who does good habits than it is to set goals.

## 7. Write about the book's perspective on how to make a habit harder to do?

- The book emphasizes the importance of making bad habits harder to do by employing strategies that reduce their attractiveness and increase the friction associated with them.

- For instance, making the cues for bad habits less visible, making the action itself unattractive and difficult, and ensuring that the reward for engaging in the bad habit is unsatisfying.

- By implementing these tactics, we can tilt the balance in favor of positive behaviors and make it easier to stick to our desired habits over time.

## 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I've been thinking about reading more regularly.

- To make it a habit, I could start by setting a specific time each day, like before bed or during my lunch break, to make the cue obvious.

- Making it more attractive could involve finding books on topics I'm genuinely interested in or joining a book club to make it a social activity.

- To make it easier, I could keep a book handy wherever I spend most of my time, like on my nightstand or in my bag.

- Finally, I could make the habit more satisfying by rewarding myself with something enjoyable after each reading session.

## 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- I've been wanting to cut down on mindlessly scrolling through social media.

- To make the cue invisible, I could delete the apps from my phone or move them to a folder on a distant screen.

- Making the process unattractive could involve reminding myself of the wasted time or the negative impact on my mental health.

- Additionally, setting specific time limits or using apps that restrict usage could make the response unsatisfying, ultimately helping me break the habit.