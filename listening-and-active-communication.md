# Listening and Active Communication

## 1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

- Avoid getting distracted by your own thoughts.

- Focus on the speaker and topic instead.

- Try not to interrupt the other person. Let them finish and then respond.

- Use door openers, these are phrases that show you're interested and keep the other person talking.

- Show that you are listening with your body language.

- If appropriate take notes during important conversations paraphrase what others have said to make sure you're both on the same page.

## 2. According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

- According to Fisher's model, reflective listening involves listening carefully to what someone says and then repeating it back in your own words.

- This shows that you understand what they're saying and helps them feel heard and valued.

- Reflective listening also involves acknowledging the speaker's feelings and emotions, which helps strengthen the connection between you and them.

- Overall, it's about being attentive, empathetic, and validating in your responses to the speaker's words and emotions.

## 3. What are the obstacles in your listening process?

- Sometimes, it's hard to really listen because our minds can get distracted.

- We might be thinking about what we want to say next or other things going on in our lives.

- Also, if we disagree with what someone is saying, it can be tough to listen without jumping in with our own opinions.

- Plus, if we're feeling stressed or tired, it's harder to focus on what someone else is saying.

- So, staying focused, being open-minded, and managing our own emotions can help us overcome these obstacles in listening.

## 4. What can you do to improve your listening?

- To get better at listening, we can try focusing more on what the person is saying.

- Pay attention to their words and feelings, and don't interrupt.

- Practice repeating back what they say to make sure you understand.

- It's important to show that you care about what they're saying by giving them your full attention.

## 5. When do you switch to Passive communication style in your day to day life?

- We might switch to passive communication when we want to avoid conflicts or when we don't feel strongly about a situation.

- It's like taking a step back and letting others take the lead without asserting yourself too much.

## 6. When do you switch into Aggressive communication styles in your day to day life?

- Aggressive communication usually kicks in when someone feels threatened, frustrated, or angry.

- It's like when you're in a rush and someone cuts in line, making you snap and react harshly.

- Or when you're arguing with someone and you start yelling or using harsh words to make your point.

- It's basically when emotions take over and you forget about being calm and respectful in your interactions.

## 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- People might switch to passive-aggressive communication when they're upset but don't feel comfortable expressing it directly.

- They might use sarcasm, gossip, taunts, or the silent treatment to indirectly show their frustration or anger.

- It's like hinting at how they feel without saying it outright.

- Sometimes, they might do this when they're afraid of conflict or unsure how to address the issue directly.

- It's best to try to communicate openly and honestly whenever possible.

## 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

- Learn to recognize and name your feelings.

- Learn to recognize and name you need.

- Start with low-stakes situation.

- Be aware of your body.

- If you have something to talk about, don't wait to speak.